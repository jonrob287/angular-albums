import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Album } from "../models/Album";

// const httpOptions = {
  // headers: new HttpHeaders({'Content-Type': 'application/json'})
// };

@Injectable({
  providedIn: 'root'
})


export class AlbumsService {
  albums: Album[]
  data:Observable<any>


  constructor() {

    this.albums =
      [
        { title: 'The Elements',
          artist: 'TobyMac',
          songs:
            [
              "The Elements",
              " I Just Need U",
              " Scars",
              " Everything",
              " Starts with Me",
              " Edge of My Seat",
              " It's You",
              " Horizon (A New Day)",
              " Hello Future",
              " Overflow",
              " See the Light"
            ],
          favorite: "The Elements",
          date: '01/01/2018',
          genre: "Christian/Gospel",
          units: 12000000,
          cover: "/assets/images/theElements.jpg",
          play: "spotify:album:5gbs9nxCzkbMdQRYXj955d",
          hide: false,
          showForm: false,
          // play: "https://open.spotify.com/embed/track/28jcFm1snS2T2CcnGladPy",
          // play: "<iframe [src]=\"https://open.spotify.com/embed/track/28jcFm1snS2T2CcnGladPy\" width=\"250\" height=\"80\" frameborder=\"0\" allowtransparency=\"true\" allow=\"encrypted-media\"></iframe>"
        },
        { title: 'Never Lose Sight',
          artist: 'Chris Tomlin',
          songs:
            [
              "Good Good Father",
              " Jesus",
              " Impossible Things",
              " Home",
              " God of Calvary",
              " He Lives",
              " Glory Be",
              " Come Thou Fount(I Will Sing)",
              " Yes and Amen",
              " All Yours",
              " First Love",
              " The God I Know",
              " Kyrie Eleison"
            ],
          favorite: "Jesus",
          date: '01/01/2016',
          genre: "Christian/Gospel",
          units: 12000000,
          cover: '/assets/images/neverLoseSight.jpg',
          hide: false,
        },
        { title: 'Stay',
          artist: 'Jeremy Camp',
          songs:
            [
              "Understand",
              " Right Here",
              " Walk By Faith",
              " Stay",
              " All the Time",
              " I Still Believe",
              " One Day at a Time",
              " Breaking My Fall",
              " Nothing",
              " I Know You're Calling",
              " Take My Life",
              " In Your Presence"
            ],
          favorite: "I Still Believe",
          date: '01/01/2002',
          genre: "Christian/Gospel",
          units: 12000000,
          cover: '/assets/images/stay.jpg',
          hide: false,
        },
        { title: 'Until the Whole World Hears',
          artist: 'Casting Crowns',
          songs: [
            " Until the Whole World Hears",
            " If We've Ever Needed You",
            " Always Enough",
            " Joyful, Joyful",
            " At Your Feet",
            " Glorious Day (Living He Loved Me)",
            " Holy One",
            " To Know You",
            " Mercy",
            " Jesus, Hold Me Now",
            " Blessed Redeemer",
            " Shadow of Your Wings"
          ],
          favorite: "Until the Whole World Hears",
          date: '01/01/2009',
          genre: "Christian/Gospel",
          units: 12000000,
          cover: '/assets/images/untilTheWholeWorldHears.jpg',
          hide: false,
        },
        { title: 'Almost There',
          artist: 'Mercy Me',
          songs: [
            "I Worship You",
            " Here Am I",
            " On My Way to You",
            " How Great Is Your Love",
            " I Can Only Imagine",
            " Bless Me Indeed (Jabez's Song)",
            " Cannot Say Enough",
            " House of God",
            " Call to Worship",
            " All Fall Down",
            " In You"
          ],
          favorite: "I Can Only Imagine",
          date: '01/01/2001',
          genre: "Christian/Gospel",
          units: 12000000,
          cover: '/assets/images/almostThere.jpg',
          hide: false,
        },
      ]//END OF ARRAY

  }// END OF CONSTRUCTOR

  getAlbums():Observable<Album[]>{
    console.log('Retrieving Albums from service.')
    return of (this.albums)
  }



  // savePost(post: Post) : Observable<Post> {
  //   return this.http.post<Post>(this.postsURL, post, httpOptions)
  // }





}//END OF CLASS



























