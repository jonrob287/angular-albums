export interface Album {
  title: string;
  artist: string;
  songs: string[];
  favorite: string;
  date: string;
  genre: string;
  units: number;
  cover?: string;
  play?: string;
  hide?: boolean;
  showForm?: boolean;
  wiki?:string;
}
