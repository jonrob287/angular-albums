export interface Footer {
  author: string;
  linkedIn: string;
  site: string;
}
