export interface Nav {
  logo: string;
  title: string;
  name: string;
}
