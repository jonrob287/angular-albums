import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { Album } from "../../models/Album";
import { Nav } from "../../models/Nav";
import { Footer } from "../../models/Footer";
import {AlbumsService} from "../../services/albums.service";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {
  @Output()newAlbumPost: EventEmitter<Album> = new EventEmitter();
  @Input()currentAlbum: Album

  album: Album ={
    title:'',
    artist:'',
    songs: [''],
    favorite:'',
    date: '',
    genre:'',
    units: null,
};
  loading: boolean = false;
  albums: Album[];
  nav: Nav[];
  footer: Footer[];
  showForm: boolean = false
  // @ViewChild('albumForm')form: any;
  data: Album

  constructor(private AlbumService: AlbumsService) {
  }
  ngOnInit(): void {
    // this.AlbumService.addAlbum(album);


    this.AlbumService.getAlbums().subscribe(albums => {
      this.albums = albums;
    })

    setTimeout(() => this.loading = true, 1000);
    this.nav =
      [
        {
          logo:'/assets/images/cross.jpeg',
          title: 'The Best Christian Albums!',
          name: 'Jonathan Robles',
        }
      ]
    this.footer = [{
      author: "Jonathan Robles",
      linkedIn: "https://www.linkedin.com/in/jonathan-robles-a33779190/",
      site: "https://gitlab.com/jonrob287"
    }]
  }// END OF ngOnInit()
  toggleHide(album: Album){
    album.hide = !album.hide;
  }
  onSubmit(e){
    console.log(123);
    e.preventDefault();
  }
  addAlbum(album: Album){
    this.albums.unshift(album);
  }

}//END OF CLASS

//
// In your albums.component.ts,
//   Create a property named 'albums' and have it reference an array of an interface named 'Album'.
//   Outside of your class, create an interface name 'Album'.
//   Your interface object should include the following properties and its datatypes
// title: string,
//   artist: string,
//   songs: string[],
//   favorite: string,
//   year: number,
//   genre: string,
//   units: number
// cover?: string
// Inside of you ngOnInit()
// Define albums
// This should be an array of objects (Album)
// Include 5 albums of your choice in your project
// In your albums.component.html
// Render the property values of every object in your array
// Include navbar and footer
// Create two components: one for a navbar, the other for a footer
// Include the selectors along with your albums'
//

